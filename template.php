<?php

function fresnocompact_theme($existing, $type, $theme, $path) {
	return array (
		'haak_nominate_entityform_edit_form' => array (
			'render element' => 'form',
			'template' => 'templates/haak_nominate_entityform',
		),
		'partnership_entityform_edit_form' => array (
			'render element' => 'form',
			'template' => 'templates/partnership_entityform',
		),
	);
}

function fresnocompact_preprocess_haak_nominate_entityform_edit_form(&$variables) {
	$variables['field_nominee_name'] = drupal_render($variables['form']['field_nominee_name']);
	$variables['field_address'] = drupal_render($variables['form']['field_address']);
	$variables['field_city'] = drupal_render($variables['form']['field_city']);
	$variables['field_zip'] = drupal_render($variables['form']['field_zip']);
	$variables['field_phone'] = drupal_render($variables['form']['field_phone']);
	$variables['field_fax'] = drupal_render($variables['form']['field_fax']);
	$variables['field_email'] = drupal_render($variables['form']['field_email']);
	$variables['field_nominated_by'] = drupal_render($variables['form']['field_nominated_by']);
	$variables['field_nominee_title'] = drupal_render($variables['form']['field_nominee_title']);
	$variables['field_nominated_by_email'] = drupal_render($variables['form']['field_nominated_by_email']);
	$variables['field_haak_description'] = drupal_render($variables['form']['field_haak_description']);
}

