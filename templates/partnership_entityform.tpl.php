<?php

/**
 * @file
 * A basic template for entityform entities
 *
 * Available variables:
 * - $content: An array of field items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The name of the entityform
 * - $url: The standard URL for viewing a entityform entity
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - entityform-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>


  <div class="content">
  	<h3>Nominate</h3>
  	<p><strong>The deadline to submit nominations for the 2019 Business Partner Award is Friday, November 16, 2018.</strong></p>
  	<div id="nominee"><?php print render ($form['field_partner_nominee']); ?></div>
  	<ul>
  		<li><?php print render ($form['field_partner_c_title']); ?></li>
  		<li><?php print render ($form['field_partner_c_phone']); ?></li>
  		<li><?php print render ($form['field_partner_c_fax']); ?></li>
  		<li><?php print render ($form['field_partner_c_email']); ?></li>
  	</ul>
  	<ul>
  		<li><?php print render ($form['field_partner_c_contact']); ?></li>
  		<li><?php print render ($form['field_partner_c_bus_address']); ?></li>
  		<li><?php print render ($form['field_partner_c_city']); ?></li>
  		<li><?php print render ($form['field_partner_c_zip']); ?></li>
  	</ul>
  	<hr class="clear">
  	<ul>
  		<li><?php print render ($form['field_nb_title']); ?></li>
  		<li><?php print render ($form['field_nb_school_district']); ?></li>
  		<li><?php print render ($form['field_nb_phone']); ?></li>
  		<li><?php print render ($form['field_nb_email']); ?></li>
  	</ul>
  	<ul>
  		<li><?php print render ($form['field_nb_name']); ?></li>
  		<li><?php print render ($form['field_nb_address']); ?></li>
  		<li><?php print render ($form['field_nb_city']); ?></li>
  		<li><?php print render ($form['field_nb_zip']); ?></li>
  	</ul>


  	<p>Describe why this business partner is deserving of recognition for the Fresno Compact Business Partner Award.</p>

  	<?php print render ($form['field_question_1']); ?>

  	<?php print render ($form['field_question_2']); ?>

  	<?php print render ($form['field_question_3']); ?>

  	<?php print drupal_render_children($form); ?>

		</div>



  </div>
