<?php

/**
 * @file
 * A basic template for entityform entities
 *
 * Available variables:
 * - $content: An array of field items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The name of the entityform
 * - $url: The standard URL for viewing a entityform entity
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - entityform-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>


  <div class="content">
  	<h3>Nominate</h3>
  	<p><strong>The deadline to submit nominations for the 2019 Dr. Harold Haak Award is Friday, December 7, 2018.</strong></p>
  	<div id="nominee"><?php print $field_nominee_name; ?></div>
  	<ul>
  		<li><?php print render ($form['field_phone']); ?></li>
  		<li><?php print render ($form['field_fax']); ?></li>
  		<li><?php print render ($form['field_email']); ?></li>
  	</ul>
  	<ul>
  		<li><?php print render ($form['field_address']); ?></li>
  		<li><?php print render ($form['field_city']); ?></li>
  		<li><?php print render ($form['field_zip']); ?></li>
  	</ul>
  	<hr class="clear">
  	<ul>
  		<li><?php print render ($form['field_nominee_title']); ?></li>
  	</ul>
  	<ul>
  		<li><?php print render ($form['field_nominated_by']); ?></li>
  		<li><?php print render ($form['field_nominated_by_email']); ?></li>
  	</ul>

  	<div id="sidebar">
	    <h3>Consideration Criteria</h3>
			<ol>
			  <li>Must be an employee of your educational organization</li>
			  <li>A track record of outstanding performance in building business/education partnerships for your organization</li>
			  <li>List example of successful partnerships to date</li>
		  </ol>
	  	<span class="sidebar-bottom">&nbsp;</span>    
  	</div>

  	<p>Please describe why this educator is deserving of recognition for outstanding performance 
  		in building business/education partnerships. <strong>Please utilize the "Consideration Criteria" 
  		located on the right.</strong>*
  	</p>

  	<div id="description">
  		<?php print render ($form['field_haak_description']); ?>
  	</div>

  	<?php print drupal_render_children($form); ?>


		</div>

  </div>
